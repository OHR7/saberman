import requests


class Game(object):
    def __init__(self, game):
        self.BASE_URL = 'https://gd2.mlb.com/components/game'
        self.GAME = game

    def get_games(self, year, month, day):
        url = '{}/{}/year_{}/month_{}/day_{}/miniscoreboard.json'.format(self.BASE_URL, self.GAME, year, month, day)
        # url = self.BASE_URL + self.GAME + '/year_' + year + '/month_' + month + '/day_' + day + '/miniscoreboard.json'
        print(url)
        response = requests.get(url)

        response = response.json()
        games = response['data']['games']['game']
        return games
